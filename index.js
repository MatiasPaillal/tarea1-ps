import express from 'express';

const app= express();
const port= process.env.PORT || 3000;

app.get('/', (req, res) => {
    res.send('¡Hola, mundo desde Express.js!');
  });
  
  // Escucha en el puerto especificado
  app.listen(port, () => {
    console.log(`Servidor Express.js en ejecución en http://localhost:${port}`);
  });